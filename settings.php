<?php

/**
 * Includes the basic scripts and markup for the header.
 */

require_once 'templates/header.php';
require_once 'scripts/library.php';

$dbextract = $conn->prepare('SELECT * FROM '.APPNAME.'_settings');
$dbextract->execute();
$appdata = $dbextract->fetch(PDO::FETCH_ASSOC);

$dbextract2 = $conn->prepare('SELECT * FROM '.APPNAME.'_likes');
$dbextract2->execute();
$pagedata = $dbextract2->fetchAll();

//var_dump($_SESSION);

?>

<div id="settings" class="container whitebg">

	<div class="page-header">
		<h1>Application Settings</h1>
	</div>
	
	<?php if($_SESSION['lock'] == NULL): ?>
	
	<div id="login">
	
		<div class="input-group">
			<span class="input-group-addon">Passphrase</span>
			<input type="text" id="pass" class="form-control" placeholder="Type in the authorization passphrase." />
		</div>
		
		<div class="btn btn-primary" onclick="authorize();">Authorize</div>
		
		<div id="report"></div>
		
		<script type="text/javascript" src="js/functions.js"></script>
		<script type="text/javascript" src="js/jCarouselite.js"></script>
		<script type="text/javascript" src="js/mCustomScrollbar.js"></script>
		
	</div>
	
	<?php else: ?>
	
	<div id="result"><p></p></div>
	
	<div id="menu">
	
		<div class="btn-group btn-group-lg">
			<button type="button" id="basic-menu" class="btn btn-default">Basic Settings</button>
			<button type="button" id="social-menu" class="btn btn-default">Social Settings</button>
			<button type="button" id="term-menu" class="btn btn-default">App Terms of Use</button>
			<button type="button" id="google-menu" class="btn btn-default">Google Analytics</button>
			<button type="button" id="databox-menu" class="btn btn-default">Databox</button>
			<button type="button" id="custom-menu" class="btn btn-default">Custom Query</button>
			<button type="button" id="statistics-menu" class="btn btn-default">Stats</button>
			<button type="button" id="export-menu" class="btn btn-default">Export</button>
		</div>
	
	</div>

	<!-- Basic info Form -->

	<div id="basic" class="col-md-offset-3 col-lg-offset-3 col-md-6 col-lg-6">
	
		<h4>Basic App settings<br /><small>Make changes to the basic settings of the application.</small></h4>
	
		<div class="input-group">
			<span class="input-group-addon">App ID:</span>
			<input type="text" id="appid" class="form-control" value="<?php print $appdata['app_id'] ?>" />
		</div>
		
		<div class="input-group">
			<span class="input-group-addon">App Secret:</span>
			<input type="text" id="appsecret" class="form-control" value="<?php print $appdata['app_secret'] ?>" />
		</div>
			
		<label class="checkbox inline">
			<input type="checkbox" id="authgate" <?php if($appdata['auth_gate'] == 1){ print 'checked="checked"'; }  ?> value="authgate"> Enable Authorization Gate
		</label>
		
		<div class="input-group">
			<span class="input-group-addon">Page Tab URL:</span>
			<input type="text" id="ptab" class="form-control" value="<?php print $appdata['page_tab_url'] ?>" />
		</div>
		
		<div class="input-group">
			<span class="input-group-addon">Shortened URL:</span>
			<input type="text" id="shorten" class="form-control" value="<?php print $appdata['shortened_url'] ?>" />
		</div>
		
		<div class="row">
			<div id="basic-btn" class="btn btn-primary glyphicon glyphicon-floppy-disk"></div> <div class="btn btn-primary" onclick="add_tab();">Add App to Page Tab</div>
		</div>
	
	</div>
	
	<!-- /Basic info Form -->
	
	<!-- Social Form -->
	
	<div id="social" class="col-md-offset-3 col-lg-offset-3 col-md-6 col-lg-6">
	
		<h4>Social Settings<br /><small>Insert or modify the text messages shared to the social networks.</small></h4>
	
		<div class="input-group">
			<span class="input-group-addon">Friend notification text:</span>
			<input type="text" id="finvite" class="form-control" value="<?php print $appdata['invite_text'] ?>" />
		</div>
		
		<div class="input-group">
			<span class="input-group-addon">Facebook Share title:</span>
			<input type="text" id="title" class="form-control" value="<?php print $appdata['share_title'] ?>" />
		</div>
		
		<div class="input-group">
			<span class="input-group-addon">Facebook Share caption:</span>
			<input type="text" id="caption" class="form-control" value="<?php print $appdata['share_caption'] ?>" />
		</div>
		
		<div class="input-group">
			<span class="input-group-addon">Facebook Share description:</span>
			<input type="text" id="desc" class="form-control" value="<?php print $appdata['share_desc'] ?>" />
		</div>
		
		<div class="input-group">
			<span class="input-group-addon">Facebook Share image:</span>
			<input type="text" id="shareimg" class="form-control" value="<?php print $appdata['share_img'] ?>"  />
		</div>
		
		<div class="row">
			<div id="social-btn" class="btn btn-primary glyphicon glyphicon-floppy-disk"></div>
		</div>
	
	</div>
	
	<!-- /Social Form -->
	
	<!-- Terms of Use Form -->
	
	<div id="term" class="col-md-offset-3 col-lg-offset-3 col-md-6 col-lg-6">
	
		<script src="js/ckeditor/ckeditor.js"></script>
		<h4>Terms of Use<br /><small>Insert the Terms of Use for the application.</small></h4>
	
		<label class="label">Terms of Use</label>
		<textarea id="app_terms" rows="15" class="form-control"><?php print $appdata['app_terms'] ?></textarea>
		<script type="text/javascript">CKEDITOR.replace('app_terms');</script>
		
		<div class="row">
			<div id="terms-btn" class="btn btn-primary glyphicon glyphicon-floppy-disk"></div>
		</div>
	
	</div>
	
	<!-- /Terms of Use Form -->
	
	<!-- Google Analytics Form -->
	
	<div id="google" class="col-md-offset-3 col-lg-offset-3 col-md-6 col-lg-6">
	
		<h4>Google Analytics<br /><small>Insert the chunk of code Google gives you to track visits to the app (without the script tags).</small></h4>

		<label class="label">Google Analytics code</label>
		<textarea id="google_code" rows="8" class="form-control"><?php print $appdata['google_analytics'] ?></textarea>
		
		<div class="row">
			<div id="google-btn" class="btn btn-primary glyphicon glyphicon-floppy-disk"></div>
		</div>
	
	</div>
	
	<!-- /Google Analytics Form -->
	
	<!-- Databox -->
	
	<div id="databox" class="col-md-12 col-lg-12">
	
	<?php
		
		try{
			$select = 'SELECT *, form_email as usermail FROM '.APPNAME.'_participations';
			$playercount = $conn->prepare($select);
			$playercount->execute();
			$players = $playercount->rowCount();
			$playerinfo = $playercount->fetchAll();
		}catch(PDOException $e){
			print 'ERROR: '.$e->getMessage();
		}
		try{
			$sumselect = 'SELECT COUNT(form_email) FROM '.APPNAME.'_participations Group by form_email';
			$uniqueplayer = $conn->prepare($sumselect);
			$uniqueplayer->execute();
			$unique = $uniqueplayer->rowCount();
			$uniqueinfo = $uniqueplayer->fetchAll();
			
		}catch(PDOException $f){
			print 'ERROR:' .$f->getMessage();
		}
		
		try{
			$shareselect = 'SELECT * FROM '.APPNAME.'_shares';
			$shareplayer = $conn->prepare($shareselect);
			$shareplayer->execute();
			$shares = $shareplayer->rowCount();
			$shareinfo = $shareplayer->fetchAll();
			
		}catch(PDOException $f){
			print 'ERROR:' .$f->getMessage();
		}
		
		
		try{
			$invselect = 'SELECT * FROM '.APPNAME.'_friends';
			$invplayer = $conn->prepare($invselect);
			$invplayer->execute();
			$invites = $invplayer->rowCount();
			$invinfo = $invplayer->fetchAll();
			
		}catch(PDOException $f){
			print 'ERROR:' .$f->getMessage();
		}
		
	?>
	
		<h4>Databox<br /><small>Information stored in the database.</small></h4>
		
		<p>Σύνολο συμμετεχόντων: <strong><?php print $players; ?></strong></p>
		<p>Σύνολο unique συμμετεχόντων: <strong><?php print $unique; ?></strong></p>
		<p>Σύνολο κοινοποιήσεων: <strong><?php print $shares; ?></strong></p>
		<p>Invites: <strong><?php print $invites; ?></strong></p>
		
		<h5>Λίστα συμμετεχόντων</h5>
		<div id="hide" class="btn btn-info">εμφάνιση/απόκρυψη</div>
		<table id="usrs" class="table table-striped">
			<tr><th>Ονοματεπώνυμο</th><th>E-mail</th><th>Newsletter</th><th>Προφίλ Facebook</th></tr>
			<?php 
			
				foreach($playerinfo as $player){
					print '<tr><td>'.$player["user_form_name"].' '.$player["user_form_lastname"].'</td><td>'.$player["usermail"].'</td><td>'.$player["user_newslet"].'</td><td><a href="'.$player["user_link"].'" target="_blank">Facebook Profile</a></td></tr>';
				}
			
			?>
		</table>
		
	
	</div>
	
	<!-- /Databox -->

	<!-- Custom Query -->
	
	<?php
		
		try{
			$select = 'SELECT form_email FROM '.APPNAME.'_participations';
			$playerexport = $conn->prepare($select);
			$playerexport->execute();
			$playerJSON = $playerexport->fetchAll();
		}catch(PDOException $e){
			print 'ERROR: '.$e->getMessage();
		}
	
	?>
	
	<div id="custom" class="col-md-12 col-lg-12">
		
		<p>
			<?php
				//var_dump($playerJSON);
				foreach($playerJSON as $player){
					$JSONstring .= $player[0].',<br>';
				}
				print $JSONstring;
			?>
		</p>
		
	</div>
	
	<!-- /Custom Query -->
	
	<!--	graph div-->
	<div id="statistics" class="col-md-12 col-lg 12">
		<br>
		<div id="allstats" class="graph"></div>
		<br>
		<div id="uniquestats" class="graph"></div>
		<br>
		<div id="socialstats" class="graph"></div>
		<?php
		
		
		try{
			$select = 'SELECT count(form_email) as number, form_date as date FROM '.APPNAME.'_participations group by form_date';
			$dataexport = $conn->prepare($select);
			$dataexport->execute();
			$data_registr = $dataexport->fetchAll();
		}catch(PDOException $e){
			print 'ERROR: '.$e->getMessage();
		}
		
				
		$participants = array();
		$dates = array();
		
		
		foreach($data_registr as $data_key=>$data_values){
				
						array_push($participants, $data_values['number']);
						array_push($dates, $data_values['date']);
						
		}
		
		
		////write to json file
		$fp = fopen('partdata.json', 'w');
		fwrite($fp, json_encode(array('date'=>$dates, 'number'=>$participants), JSON_NUMERIC_CHECK));
		fclose($fp);
	
		
		try{
			$select = 'SELECT count(DISTINCT form_email) as numbers, form_date as dates FROM '.APPNAME.'_participations group by form_date';
			$data_unique_export = $conn->prepare($select);
			$data_unique_export->execute();
			$data_unique_registr = $data_unique_export->fetchAll();
		}catch(PDOException $e){
			print 'ERROR: '.$e->getMessage();
		}			
						
		$unique_participants = array();
		$unique_dates = array();
		
		foreach($data_unique_registr as $data_unique_key=>$data_unique_values){
						
				array_push($unique_participants, $data_unique_values['numbers']);
				array_push($unique_dates, $data_unique_values['dates']);
		
		}
		
		////write to json file
		$f = fopen('uniquedata.json', 'w');
		fwrite($f, json_encode(array('dates'=>$unique_dates, 'numbers'=>$unique_participants), JSON_NUMERIC_CHECK));
		fclose($f);
		
		
		?>
		
	</div>
	<!--	export Table to csv-->
	<div id="export" class="col-md-12 col-lg 12">
		<br>
		<button id="unique" class="expbtn btn btn-primary" onclick="location.href='export.php'">Export Total Entries</button>
		<button id="expunique" class="expbtn btn btn-info" onclick="location.href='expunique.php'">Export Unique Entries</button>
	</div>
	
	<div class="col-md-12 col-lg-12">
	<hr />
	<p class="text-info">Powered by Shutterspeed v1.1 framework</p>
	<script type="text/javascript" src="js/functions.js"></script>
	<script type="text/javascript" src="js/jCarouselite.js"></script>
	<script type="text/javascript" src="js/mCustomScrollbar.js"></script>
	</div>
	
	
	
	
</div>

<script type="text/javascript">

	$(document).ready(function(){
		
		$('#social').hide();
		$('#term').hide();
		$('#google').hide();
		$('#databox').hide();
		$('#custom').hide();
		$('#statistics').hide();
		$('#export').hide();
		
		$('#hide').click(function(){
			$('#usrs').toggle();
		});
		
		if($('#likegate').attr('checked') == undefined){ $('#pages').hide(); }
		
		$('#likegate').click(function(){
			
			if($('#likegate').attr('checked') == undefined){ $('#pages').hide(); }else{ $('#pages').show(); }
		
		});
		
		$('.delpage-btn').click(function(){
			
			var check = confirm('Are you sure you want to delete this page?');
			
			if(check == true){
				
				var pageid = $(this).parent('well').find('input.pageid').val();
				
				console.log(pageid);
				
				var del = {};
				del['req'] = 'delete_page';
				del['pageid'] = pageid;
				
				$.post('scripts/helpers/ajax_calls.php', del, function(ret){
			
					if(ret == true){
						$('#result p').removeAttr('class').addClass('alert alert-success col-md-12 col-lg-12').html('Page deleted.');
						$(this).parent('blockquote').fadeOut(2000);
					}else{
						$('#result p').removeAttr('class').addClass('alert alert-danger col-md-12 col-lg-12').html('<strong>Warning!</strong> there was an error processing the request. The script reported:<br /><pre>'+ret+'</pre>');
					}
					
				});
					
			}
		
		});
		
		$('.btn-group button').click(function(){
			
			var choice = $(this).attr('id').substring(0, $(this).attr('id').length-5);
			
			hideall();
			
			$('#'+choice).show();
			
		});
		
		$('#basic-btn').click(function(){
			
			var basics = {};
			var pnames;
			var purls;
			var pages = {};
			
			basics['req'] = 'db_in_basics';
			basics['appid'] = $('#appid').val();
			basics['appsecret'] = $('#appsecret').val();
			
			if($('#authgate').attr('checked') == undefined){ basics['authgate'] = 0 }else{ basics['authgate'] = 1 };
			basics['ptab'] = $('#ptab').val();
			basics['shorten'] = $('#shorten').val();
			
			$.post('scripts/helpers/ajax_calls.php', basics, function(ret){
			
				if(ret == true){
					$('#result p').removeAttr('class').addClass('alert alert-success col-md-12 col-lg-12').html('Settings saved successfully.');
				}else{
					$('#result p').removeAttr('class').addClass('alert alert-danger col-md-12 col-lg-12').html('<strong>Warning!</strong> there was an error processing the request. The script reported:<br /><pre>'+ret+'</pre>');
				}
				
			});
			
		});
		
		$('#addpage-btn').click(function(){
			
			$('#pageswrap').append('<blockquote><div class="input-group"><span class="input-group-addon">Page ID:</span><input type="text" class="form-control pageid" /></div><div class="input-group"><span class="input-group-addon">Name:</span><input type="text" class="form-control pagename" /></div><div class="input-group"><span class="input-group-addon">Page Facebook URL:</span><input type="text" class="form-control pageurl" /></div></blockquote>');
		
		});
		
		$('#social-btn').click(function(){
			
			var social = {};
			
			social['req'] = 'db_in_social';
			social['finvite'] = $('#finvite').val();
			social['title'] = $('#title').val();
			social['caption'] = $('#caption').val();
			social['desc'] = $('#desc').val();
			social['shareimg'] = $('#shareimg').val();
			
			$.post('scripts/helpers/ajax_calls.php', social, function(ret){
			
				if(ret == true){
					$('#result p').removeAttr('class').addClass('alert alert-success col-md-12 col-lg-12').html('Settings saved successfully.');
				}else{
					$('#result p').removeAttr('class').addClass('alert alert-danger col-md-12 col-lg-12').html('<strong>Warning!</strong> there was an error processing the request. The script reported:<br /><pre>'+ret+'</pre>');
				}
				
			});
			
		});
		
		$('#terms-btn').click(function(){
			
			var terms = {};
			
			terms['req'] = 'db_in_terms';
			terms['terms'] = CKEDITOR.instances['app_terms'].getData();
			
			$.post('scripts/helpers/ajax_calls.php', terms, function(ret){
			
				console.log(ret);
				if(ret == true){
					$('#result p').removeAttr('class').addClass('alert alert-success col-md-12 col-lg-12').html('Settings saved successfully.');
				}else{
					$('#result p').removeAttr('class').addClass('alert alert-danger col-md-12 col-lg-12').html('<strong>Warning!</strong> there was an error processing the request. The script reported:<br /><pre>'+ret+'</pre>');
				}
				
			});
			
		});
		
		$('#google-btn').click(function(){
			
			var google = {};
			
			google['req'] = 'db_in_google';
			google['google'] = $('#google_code').val();
			
			$.post('scripts/helpers/ajax_calls.php', google, function(ret){
			
				if(ret == true){
					$('#result p').removeAttr('class').addClass('alert alert-success col-md-12 col-lg-12').html('Settings saved successfully.');
				}else{
					$('#result p').removeAttr('class').addClass('alert alert-danger col-md-12 col-lg-12').html('<strong>Warning!</strong> there was an error processing the request. The script reported:<br /><pre>'+ret+'</pre>');
				}
				
			});
			
		});
});
	
	
//	draw charts
//total participation per day chart
	$.getJSON("http://apps.xplain.co/<?php echo APPNAME?>/partdata.json", function(datajson) {
		$('#allstats').highcharts({
			title: {
				text: 'App participations per day',
				x: -20 //center
			},
			subtitle: {
				text: '',
				x: -20
			},
			xAxis: {
				categories: datajson['date']
			},
			yAxis: {
				title: {
					text: 'Participations'
				},
				plotLines: [{
					value: 0,
					width: 1,
					color: '#808080'
				}]
			},
			tooltip: {
				valueSuffix: ' people'
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'middle',
				borderWidth: 0
			},
			series: [{
				name: 'Participants',
				data: datajson['number']
			}]
			});
		});
	
//	unique participation per day chart
	$.getJSON("http://apps.xplain.co/<?php echo APPNAME?>/uniquedata.json", function(uniquejson) {
		console.log(uniquejson);
		$('#uniquestats').highcharts({
			title: {
				text: 'App unique participations per day',
				x: -20 //center
			},
			subtitle: {
				text: '',
				x: -20
			},
			xAxis: {
				categories: uniquejson['dates']
			},
			yAxis: {
				title: {
					text: 'Unique Participations'
				},
				plotLines: [{
					value: 0,
					width: 1,
					color: '#808080'
				}]
			},
			tooltip: {
				valueSuffix: ' people'
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'middle',
				borderWidth: 0
			},
			series: [{
				name: 'Participants',
				data: uniquejson['numbers']
			}]
			});
		});	
	
	
//	all social stats
//$(function () {
//    // Create the chart
//    $('#socialstats').highcharts({
//        chart: {
//            type: 'column'
//        },
//        title: {
//            text: 'All App Registrations, Shares and Invites'
//        },
//        subtitle: {
//            text: ''
//        },
//        xAxis: {
//            type: 'category'
//        },
//        yAxis: {
//            title: {
//                text: 'Number of users'
//            }
//
//        },
//        legend: {
//            enabled: false
//        },
//        plotOptions: {
//            series: {
//                borderWidth: 0,
//                dataLabels: {
//                    enabled: true,
//                    format: '{point.y:.1f}%'
//                }
//            }
//        },
//
//        tooltip: {
//            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
//            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
//        },
//
//        series: [{
//            name: 'Brands',
//            colorByPoint: true,
//            data: [{
//                name: 'Total sign ins',
//                y: 56.33
//            }, {
//                name: 'Shares',
//                y: 24.03
//            }, {
//                name: 'User invitations',
//                y: 10.3
//            }, {
//                name: 'Done Nothing',
//                y: 4.77
//            }]
//        }],
//     
//    });
//});
	
	
	
	function hideall(){
		$('#basic').hide();
		$('#social').hide();
		$('#term').hide();
		$('#google').hide();
		$('#databox').hide();
		$('#custom').hide();
		$('#statistics').hide();
		$('#export').hide();
	}

</script>

<?php endif; ?>