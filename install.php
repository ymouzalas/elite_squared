<?php

/**
 * Includes the basic scripts and markup for the header.
 */

include 'templates/header.php';

?>

<div id="installer" class="container">

	<div class="page-header">
		<h1>Shutterspeed v1.1 Installation<br /><small>Basic Database Information - Step 1/2</small></h1>
	</div>
	
	<div id="result" class="row"><p></p></div>
	
	<div id="heading" class="row"><p class="col-md-offset-3 col-lg-offset-3 col-md-6 col-lg-6">It is time to set up the basic information of your Application. Fill in the form and save to complete the installation process!</p></div>

	<!-- Database Form -->

	<div id="dbform" class="col-md-offset-3 col-lg-offset-3 col-md-6 col-lg-6">
	
		<div class="input-group">
			<span class="input-group-addon">App machine name:</span>
			<input type="text" id="appname" class="form-control" placeholder="Appname" />
		</div>
		
		<div class="input-group">
			<span class="input-group-addon">DB Username:</span>
			<input type="text" id="dbuname" class="form-control" placeholder="Username" />
		</div>
		
		<div class="input-group">
			<span class="input-group-addon">DB Password:</span>
			<input type="password" id="dbpass" class="form-control" placeholder="Password" />
		</div>
		
		<div class="row">
			<div class="col-lg-6">
				<div class="input-group">
					<span class="input-group-addon">Host:</span>
					<input type="text" value="localhost" id="dbhost" class="form-control" placeholder="localhost" />
				</div>
			</div>
			
			<div class="col-lg-6">
				<div class="input-group">
					<span class="input-group-addon">Port:</span>
					<input type="text" value="3307" id="dbport" class="form-control" placeholder="3306" />
				</div>
			</div>
		</div>
		
		<div class="input-group">
			<span class="input-group-addon">Database Name:</span>
			<input type="text" id="dbname" class="form-control" />
		</div>
		
		<div class="row">
			<div id="install-btn" class="btn btn-primary glyphicon glyphicon-floppy-disk"></div>
		</div>
	
	</div>
	
	<!-- /Database Form -->
	
	<!-- Setup Form -->
	
	<div id="setupform" class="col-md-offset-3 col-lg-offset-3 col-md-6 col-lg-6">
	
		<div class="input-group">
			<span class="input-group-addon">Passphrase:</span>
			<input type="text" id="pass" class="form-control" placeholder="A passphrase to access the app's settings" />
		</div>
		
		<div class="input-group">
			<span class="input-group-addon">App ID:</span>
			<input type="text" id="appid" class="form-control" placeholder="Facebook App ID" />
		</div>
		
		<div class="input-group">
			<span class="input-group-addon">App Secret:</span>
			<input type="text" id="appsecret" class="form-control" placeholder="Facebook App Secret" />
		</div>
		
		<!-- <label class="checkbox inline">
			<input type="checkbox" id="likegate" value="likegate"> Enable Like Gate
		</label> -->
			
		<label class="checkbox inline">
			<input type="checkbox" id="authgate" value="authgate"> Enable Authorization Gate
		</label>
		
		<div class="row">
			<div id="save-app" class="btn btn-primary glyphicon glyphicon-floppy-disk"></div>
		</div>
	
	</div>
	
	<!-- /Setup Form -->
	
</div>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/jCarouselite.js"></script>
<script type="text/javascript" src="js/mCustomScrollbar.js"></script>
<script type="text/javascript">

	$(document).ready(function(){
		
		$('#setupform').hide();
		$('#heading p').hide();
		
		//Database installation script.
		
		$('#install-btn').click(function(){
			
			var dbdata = new Array();
			
			dbdata['appname'] = $('#appname').val();
			dbdata['host'] = $('#dbhost').val();
			dbdata['port'] = $('#dbport').val();
			dbdata['name'] = $('#dbname').val();
			dbdata['user'] = $('#dbuname').val();
			dbdata['pass'] = $('#dbpass').val();
			
			$.post('scripts/helpers/dbtest.php', {'appname' : dbdata['appname'], 'host' : dbdata['host'], 'port' : dbdata['port'], 'name' : dbdata['name'], 'user' : dbdata['user'], 'pass' : dbdata['pass']}, function(ret){
				
				if(ret != true){
					
					$('#result p').removeAttr('class').addClass('alert alert-danger col-md-12 col-lg-12').html('<strong>Warning!</strong> there was an error in the database creation. The script reported:<br /><pre>'+ret+'</pre>');
					
				}else{
					
					$('#result p').removeAttr('class').addClass('alert alert-success col-md-12 col-lg-12').html('<strong>Great!</strong> The database installation completed successfully!').delay(2000).fadeOut(2000);
					
					$('#dbform').fadeOut(4000);
					$('#setupform').delay(3500).fadeIn(4000);
					
					$('.page-header small').html('Application setup - Step 2/2');
					
					$('#heading p').delay(4000).fadeIn(2000);
					
				}
			
			});
			
		});
		
		//Application setup script.
		
		$('#save-app').click(function(){
			
			var setdata = new Array();
			
			setdata['pass'] = $('#pass').val();
			setdata['appid'] = $('#appid').val();
			setdata['secret'] = $('#appsecret').val();
			if($('#likegate').attr('checked') == undefined){ setdata['likegate'] = 0 }else{ setdata['likegate'] = 1 };
			if($('#authgate').attr('checked') == undefined){ setdata['authgate'] = 0 }else{ setdata['authgate'] = 1 };
			
			$.post('scripts/helpers/saveSettings.php', { 'pass': setdata['pass'], 'appid': setdata['appid'], 'secret': setdata['secret'], 'likegate': setdata['likegate'], 'authgate': setdata['authgate'] }, function(ret){
			
				if(ret == true){
					
					$('#setupform').fadeOut(3000);
					$('#heading p').fadeOut(3000);
					
					$('#result p').removeAttr('class').addClass('alert alert-success col-md-12 col-lg-12').html('<strong>Congratulations!</strong> The settings were stored for your application. Click <a href="index.php">here</a> to go to the first page of the app or click <a href="settings.php">here</a> to go to the settings page.').fadeIn(3000);
					
				}
				
			});
		
		});
		
	});

</script>