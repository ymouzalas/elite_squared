<?php

/**
 * The main class of the framework.
 * Contains usefull methods for working with the database
 * and Facebook's API.
 *
 * @author Zisis Koukoumakis (koykis@gmail.com)
 * @copyright Athens, 2015
 */

class Shutterspeed {
	
	public function getSettings(){
		
		global $conn;
		
		try{
			$select = "SELECT * FROM ".APPNAME."_settings";
			$query = $conn->prepare($select);
			$query->execute();
			$sets = $query->fetch(PDO::FETCH_ASSOC);
		}catch(PDOException $e){
			print 'ERROR: '.$e->getMessage();
		}
		
		return $sets;
		
	}
	
	/**
	 * The auth method checks and declares if the user has authorized the application.
	 * Input variables  : NONE.
	 * Output variables : $auth (bool | true if user has authorized / false if not ).
	 * 
	 */
	
	public function auth($uid) {
    
		$auth = false;
		
		if(isset($_SESSION['logged'])){ //Non-authenticated users have no cookie
			$auth = true;
		}
		
		return $auth;
		
	}
	
	/**
	 * The insertDB method is used to store information we need into our database for faster access and to generate
	 * less traffic with the facebook API.
	 * Input variables  : $table (string | The table name for the information to be stored).
	 *										$fields (array | The list of fields in your database table).
	 *										$values (array | The corresponding value for each field to be filled).
	 * Output variables :	$inserted (bool | true if the operation succeded / false if not).
	 *
	 * WARNING(1): User information stored from Facebook's database should not be used for commercial purposes, since it is illegal.
	 * WARNING(2): Images can but SHOULD NOT be stored in databases since they make it too heavy and slow.
	 * 
	 */
	
	public function insertDB($table, $fields, $values){
		global $conn;
		$inserted = false;
		$fieldsList = '';
		$valueNum = '';
		
		foreach($fields as $field){
			$fieldsList .= $field.', ';
			$valueNum .= '?, ';
		}
		
		$fieldsList = substr($fieldsList, 0, -2); //removes the last two characters from the string
		$valueNum = substr($valueNum, 0, -2);
		
		try{
			$query = $conn->prepare("INSERT INTO ".$table." (".$fieldsList.") VALUES (".$valueNum.")");
			$query->execute($values);
			$inserted = true;
		}catch(PDOException $e){
			return 'ERROR: '.$e->getMessage();
		}
		
		return $inserted;
	}
	
	/**
	 * The updateDB method is used to update stored information in our database.
	 * Input variables  : $table (string | The name of the table to be updated).
	 *										$fields (array | The list of fields in your database table).
	 *										$values (array | The corresponding value for each field to be filled).
	 * Output variables :	$updated (bool | true if the operation succeded / false if not).
	 * 
	 */
	
	public function updateDB($table, $fields, $values){
		global $conn;
		$updated = false;
		$fieldsList = '';
		
		foreach($fields as $field){
			$fieldsList .= $field.' = ?, ';
		}
		
		$fieldsList = substr($fieldsList, 0, -2);
		
		try{
			$query = $conn->prepare("UPDATE ".$table." SET ".$fieldsList." WHERE passphrase = ?");
			$query->execute($values);
			$updated = true;
		}catch(PDOException $e){
			return 'ERROR: '.$e->getMessage();
		}
		
		return $updated;
	}
	
	/**
	 * The countParticipations method counts the rows of the participations table where the current user's id is present.
	 * It then returns the result.
	 * Input variables  : $table (string | The table name for the information to be stored).
	 *										$user (int | The logged in user's Facebook ID).
	 * Output variables :	$count (int | the number of rows in the participation table).
	 * 
	 */
	
	public function countParticipations($table, $user){
		global $conn;
		
		try{
			$select = "SELECT * FROM ".$table." WHERE user_fb_id = ?";
			$query = $conn->prepare($select);
			$query->execute(array($user));
			$count = $query->rowCount();
		}catch(PDOException $e){
			print 'ERROR: '.$e->getMessage();
		}
		
		return $count;
	}
	
	/**
	 * The listParticipations method collects all the data from the current user's participation table.
	 * Then returns it in an array.
	 * Input variables  : $table (string | The table name for the information to be stored).
	 *										$user (int | The logged in user's Facebook ID).
	 *										$params (string | Additional parameters we might want to add to the query's WHERE part).
	 * Output variables :	$recieved (array | All the data from the current user's participation table).
	 * 
	 */
	
	public function listParticipations($table, $user, $params){
		global $conn;
		
		try{
			$select = "SELECT * FROM ".$table." WHERE user_fb_id=? ".$params;
			$query = $conn->prepare($select);
			$query->execute(array($user));
			$recieved=$query->fetchAll();
		}catch(PDOException $e){
				print 'ERROR: '.$e->getMessage();
		}
		
		return $recieved;
	}
	
	/**
	 * The parsePageSignedRequest method collects the information passed by Facebook into the page tab's iframe and
	 * returns an array with all the information collected. (This one was leeched from somewhere in the web, congratulations to the creator).
	 * Input variables  : NONE.
	 * Output variables :	$data (array | the data from Facebook's signed request / false on failure ).
	 * 
	 */
	
	public function parsePageSignedRequest() {
		
		 if (isset($_REQUEST['signed_request'])) {
			 $encoded_sig = null;
			 $payload = null;
			 list($encoded_sig, $payload) = explode('.', $_REQUEST['signed_request'], 2);
			 $sig = base64_decode(strtr($encoded_sig, '-_', '+/'));
			 $data = json_decode(base64_decode(strtr($payload, '-_', '+/'), true));
			 return $data;
		 }
		 return false;
		 
	}
	
	/**
	 * The editImgs method is mainly used to create images that will be automatically uploaded to the user's profile.
	 * It consists of many parts and it is strongly recommended that it sould be edited, at least for the early days of
	 * the framework's development so that it suits your needs. It includes native PHP functions to merge images.
	 * WARNING: Before editing, make sure you have a basic understanding of what functions like imagecopymerge() do and how.
	 * 
	 * Input variables  : $user (int | The Facebook User ID of the current user).
	 *										$bg (string | URL of the background picture, as a JPEG type).
	 *										$image (string | URL of the image to be merged with the background).
	 *										$coordX (int | Horizontal coordinate from the left edge of the background, where the target image will be placed).
	 *										$coordY (int | Vertical coordinate from the top edge of the background, where the target image will be placed).
	 * Output variables :	$user (int | The Facebook User ID of the current user - same as user's input variable).
	 * 										A file with the user's ID as its name and the suffix .png in the folder img/generated (ex. 287654321.png).
	 * 
	 * WARNING: The generated folder MUST have 777 permissions so that the script can create the file.
	 */
	
	public function editImgs($user, $bg, $image, $coordX, $coordY){
		
		$imgbg = imagecreatefromjpeg($bg);
		
		$imgadd = imagecreatefromjpeg($image);
			
		imagecopymerge($imgbg, $imgadd, $coordX, $coordY, 0, 0, imagesx($imgadd), imagesy($imgadd), 100);
		
		imagepng($imgbg, 'img/generated/'.$user.'.png');
		
		ImageDestroy($imgbg);
		
		return $user;
		
	}
	
	/**
	 * The editPngs method is mainly used to create images that will be automatically uploaded to the user's profile.
	 * It consists of many parts and it is strongly recommended that it sould be edited, at least for the early days of
	 * the framework's development so that it suits your needs. It includes native PHP functions to merge images.
	 * WARNING: Before editing, make sure you have a basic understanding of what functions like imagecopymerge() do and how.
	 * 
	 * Input variables  : $user (int | The Facebook User ID of the current user).
	 *										$bg (string | URL of the background picture, as a PNG type).
	 *										$image (string | URL of the image to be merged with the background).
	 *										$coordX (int | Horizontal coordinate from the left edge of the background, where the target image will be placed).
	 *										$coordY (int | Vertical coordinate from the top edge of the background, where the target image will be placed).
	 * Output variables :	$user (int | The Facebook User ID of the current user - same as user's input variable).
	 * 										A file with the user's ID as its name and the suffix .png in the folder img/generated (ex. 287654321.png).
	 * 
	 * WARNING: The generated folder MUST have 777 permissions so that the script can create the file.
	 */
	
	public function editPngs($user, $bg, $image, $coordX, $coordY){
		
		$imgbg = imagecreatefrompng($bg);
		
		$imgadd = imagecreatefromjpeg($image);
			
		imagecopymerge($imgbg, $imgadd, $coordX, $coordY, 0, 0, imagesx($imgadd), imagesy($imgadd), 100);
		
		imagepng($imgbg, 'img/generated/'.$user.'.png');
		
		ImageDestroy($imgbg);
		
		return $user;
		
	}
	
	/**
	 * A simple method to generate a personal code for the user, intended for any use where you need a unique identifier for the user.
	 * Since it is MD5 encoded, it cannot be recognized.
	 * Input variables  : NONE.
	 * Output variables :	$id (string | The unique identifier generated).
	 */
	 
	public function personalCode(){
		$id = md5(uniqid(time().session_id()));
		return $id;
	}
	
	/**
	 * bit.ly PHP url shortening method. This should be enough to give you an idea of what it does. Gets your bit.ly credentials and the
	 * URL you want to shorten as input and outputs a shortened URL for any use.
	 *
	 * The URL to get the appKey from: https://bitly.com/a/your_api_key.
	 */
		
	
	public function get_bitly_short_url($url, $login, $appkey) {
	
		$ch = curl_init('http://api.bitly.com/v3/shorten?login='.$login.'&apiKey='.$appkey.'&longUrl='.$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		 
		$result = json_decode(curl_exec($ch));
		return $result->data->url; //returns the shortened url as string
	}
	
}