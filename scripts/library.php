<?php

/**
 * This file controls the data of the Shutterspeed project, initiates all the objects and
 * connections with the Database and other files required for the app to run.
 *
 */
 
header('p3p: CP="NOI ADM DEV PSAi COM NAV OUR OTR STP IND DEM HONK CAO PSA OUR"'); //Required for Facebook apps to run on Microsoft Internet Explorer

date_default_timezone_set('Europe/Athens');

session_start();

// We include the apropriate libraries to work with in the app.

require_once "inc/db_settings.php";
require_once "inc/Mobile_Detect.php";
require_once "inc/fb_init.php";
require_once "inc/Shutterspeed.php";

// Establishing connection with the database.

$host='mysql:host='.DBHOST.';port='.DBPORT.';dbname='.DBNAME.';';
try{
	$conn = new PDO($host, DBUSER, DBPASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e){
	print 'ERROR: '.$e->getMessage();
}

// Initializing the objects we are going to use.

$device = new Mobile_Detect();

if(isset($_GET['m'])){
	if(!$device->isMobile()){
		header('Location: https://www.facebook.com/');
	}
}

$app = new Shutterspeed();

$sets = $app->getSettings();
