<?php

$name = $_POST['appname'];
$host = $_POST['host'];
$port = $_POST['port'];
$dbname = $_POST['name'];
$user = $_POST['user'];
$pass = $_POST['pass'];

try{
	$conn = new pdo( 'mysql:host='.$host.';port='.$port.';dbname='.$dbname,
                  $user,
                  $pass,
                  array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
	
	try{
		$name .= '_settings';
		$query = $conn->prepare('CREATE TABLE IF NOT EXISTS '.$name.' (
															passphrase varchar(35) COLLATE utf8_unicode_ci NOT NULL,
															app_id varchar(25) COLLATE utf8_unicode_ci NOT NULL,
															app_secret varchar(50) COLLATE utf8_unicode_ci NOT NULL,
															like_gate tinyint(1) NOT NULL,
															auth_gate tinyint(1) NOT NULL,
															page_tab_url text COLLATE utf8_unicode_ci,
															shortened_url text COLLATE utf8_unicode_ci,
															google_analytics text COLLATE utf8_unicode_ci,
															invite_text text COLLATE utf8_unicode_ci,
															share_title varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
															share_caption varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
															share_desc varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
															share_img text COLLATE utf8_unicode_ci,
															app_terms longtext COLLATE utf8_unicode_ci,
															PRIMARY KEY (passphrase)
														) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;');
		$query->execute();
		$name = $_POST['appname'];
	}catch(PDOException $e){
		print 'ERROR: '.$e->getMessage();
	}
					
	try{
		$name .= '_users';
		$query = $conn->prepare('CREATE TABLE IF NOT EXISTS '.$name.' (
															user_fb_id varchar(35) COLLATE utf8_unicode_ci NOT NULL,
															user_firstname text COLLATE utf8_unicode_ci NOT NULL,
															user_lastname text COLLATE utf8_unicode_ci NOT NULL,
															user_email text COLLATE utf8_unicode_ci,
															user_gender varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
															user_link text COLLATE utf8_unicode_ci NOT NULL,
															user_dev text COLLATE utf8_unicode_ci NULL,
															PRIMARY KEY (user_fb_id)
														) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;');
		$query->execute();
		$name = $_POST['appname'];
	}catch(PDOException $e){
		print 'ERROR: '.$e->getMessage();
	}
	
	try{
		$name .= '_participations';
		$query = $conn->prepare('CREATE TABLE IF NOT EXISTS '.$name.' (
															part_id INT not null AUTO_INCREMENT,
															user_fb_id varchar(35) COLLATE utf8_unicode_ci NOT NULL,
															user_form_name varchar(55) COLLATE utf8_unicode_ci NOT NULL,
															user_form_lastname varchar(55) COLLATE utf8_unicode_ci NOT NULL,
															form_email longtext COLLATE utf8_unicode_ci NOT NULL,
															user_newslet varchar(10) COLLATE utf8_unicode_ci NOT NULL,
															form_date date NOT NULL,
															form_time time NOT NULL,
															PRIMARY KEY (part_id)
														) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;');
		$query->execute();
		$name = $_POST['appname'];
	}catch(PDOException $e){
		print 'ERROR: '.$e->getMessage();
	}
	
	try{
		$name .= '_friends';
		$query = $conn->prepare('CREATE TABLE IF NOT EXISTS '.$name.' (
															user_fb_id varchar(35) NOT NULL,
															friend_id varchar(35) NOT NULL,
															date date NOT NULL,
															time time NOT NULL,
															PRIMARY KEY (user_fb_id, friend_id)
														) CHARACTER SET utf8 COLLATE utf8_unicode_ci');
		$query->execute();
		$name = $_POST['appname'];
	}catch(PDOException $e){
		print 'ERROR: '.$e->getMessage();
	}
	
	try{
		$name .= '_shares';
		$query = $conn->prepare('CREATE TABLE IF NOT EXISTS '.$name.' (
														 user_fb_id VARCHAR( 35 ) NOT NULL ,
														 share_id VARCHAR( 35 ) NOT NULL ,
														 date DATE NOT NULL ,
														 time TIME NOT NULL ,
														PRIMARY KEY (  user_fb_id ,  share_id )
														) CHARACTER SET utf8 COLLATE utf8_unicode_ci');
		$query->execute();
		$name = $_POST['appname'];
	}catch(PDOException $e){
		print 'ERROR: '.$e->getMessage();
	}
	
	try{
		$name .= '_likes';
		$query = $conn->prepare('CREATE TABLE IF NOT EXISTS '.$name.' (
															page_id varchar(35) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
															page_name text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
															page_url varchar(120) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
															page_clicks int(6) NOT NULL,
															PRIMARY KEY (page_id)
														) ENGINE=MyISAM  DEFAULT CHARSET=utf8;');
		$query->execute();
		$name = $_POST['appname'];
	}catch(PDOException $e){
		print 'ERROR: '.$e->getMessage();
	}
	
	chmod('../inc/db_settings.php', 0777);
	
	file_put_contents('../inc/db_settings.php', '<?php define("DBHOST", "'.$host.'");
	define("DBPORT", "'.$port.'");
	define("DBNAME", "'.$dbname.'");
	define("DBUSER", "'.$user.'");
	define("DBPASS", "'.$pass.'");
	define("APPNAME", "'.$name.'"); ?>');
	
	print true;
}
catch(PDOException $ex){
	die('Error: '.$ex);
}
