<?php

require_once '../library.php';

$req = $_POST['req'];

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') { // AJAX check
	
	switch($req){
		
		//Save basic app settings - Called by: settings.php
		case 'db_in_basics':
		
			$cookie = $app->updateDB(APPNAME.'_settings', array('app_id', 'app_secret', 'like_gate', 'auth_gate', 'page_tab_url', 'shortened_url'), array($_POST['appid'], $_POST['appsecret'], $_POST['likegate'], $_POST['authgate'], $_POST['ptab'], $_POST['shorten'], $sets['passphrase']));
			print $cookie;
			break;
		
		//Save interacting pages settings - Called by: settings.php
		case 'db_in_pages':
		
			$ids = explode('|', $_POST['ids']);
			$names = explode('|', $_POST['names']);
			$urls = explode('|', $_POST['urls']);
			$cnt = 0;
			
			foreach($ids as $id){
				$app->insertDB(APPNAME.'_likes', array('page_id', 'page_name', 'page_url', 'page_clicks'), array($id, $names[$cnt], $urls[$cnt], 0));
				$cnt++;
			}
			break;
			
		//Delete an interacting page - Called by: settings.php
		case 'delete_page':
		
			$dbdelete = $conn->prepare('DELETE FROM '.APPNAME.'_likes WHERE page_id = "'.$_POST['pageid'].'"');
			$cookie = $dbdelete->execute();
			print $cookie;
			break;
					
		//Save social app settings - Called by: settings.php
		case 'db_in_social':
		
			$cookie = $app->updateDB(APPNAME.'_settings', array('invite_text', 'share_title', 'share_caption', 'share_desc', 'share_img'), array($_POST['finvite'], $_POST['title'], $_POST['caption'], $_POST['desc'], $_POST['shareimg'], $sets['passphrase']));
			print $cookie;
			break;
			
		//Save app terms settings - Called by: settings.php
		case 'db_in_terms':
		
			$cookie = $app->updateDB(APPNAME.'_settings', array('app_terms'), array($_POST['terms'], $sets['passphrase']));
			print $cookie;
			break;
			
		//Save google analytics code - Called by: settings.php
		case 'db_in_google':
		
			$cookie = $app->updateDB(APPNAME.'_settings', array('google_analytics'), array($_POST['google'], $sets['passphrase']));
			print $cookie;
			break;
		
		case 'user_login':
			
			$_SESSION['logged'] = $_POST['logged'];
			$_SESSION['firstName'] = $_POST['firstName'];
			$_SESSION['lastName'] = $_POST['lastName'];
			$_SESSION['gender'] = $_POST['gender'];
			$_SESSION['email'] = $_POST['email'];
			break;
		
		//Pass the app settings and essential information to the jsdk.js file - Called by: jsdk.js
		case 'stats_request':
		
			$dbextract2 = $conn->prepare('SELECT * FROM '.APPNAME.'_likes');
			$dbextract2->execute();
			$pagedata = $dbextract2->fetchAll();
		
			$settings = $sets;
			$settings[] = APPNAME;
			foreach($pagedata as $page){
				$settings[] = $page['page_url'];
			}
			print implode("2α4ς", $settings);
			break;
		
		//Check the password from the database - Called by: settings.php
		case 'check_pass':
		
			if($_POST['pass'] === $sets['passphrase']){
				$_SESSION['lock'] = true;
				print 'Login successfull, click <a href="settings.php">HERE</a> to proceed.';
			}else{
				print '<strong>Wrong passphrase!</strong> Please try again.';
			}
			break;
			
		//Register a like in the database when the user clicks the FB like button - Called by: jsdk.js
		case 'inc_likes':
		
			$page = $_POST['page'];
			
			$q = "UPDATE ".APPNAME."_likes SET page_clicks = page_clicks + 1 WHERE page_url = '".$page."'";
			try{
				$query = $conn->prepare($q);
				$query->execute();
				$mad = $query->rowCount();
			}catch(PDOException $e){
				print 'ERROR: '.$e->getMessage();
			}
			
			print $q.'\n';
			print $mad.' - EOF';
			break;
			
		//Check if the user fits through the like gate - Called by: jsdk.js on behalf of index.php?page=like (templates/like.php)
		case 'like_check':
			
			print $app->extlike($token);
			break;
			
		//Get the friends the user has already sent an app request - Called by: functions.js on behalf of any page that has an app request button
		case 'read_friends':

			try{
				$select = "SELECT * FROM ".APPNAME."_friends WHERE user_fb_id = ?";
				$query = $conn->prepare($select);
				$query->execute(array($_SESSION['logged']));
				$invites = $query->fetchAll();
			}catch(PDOException $e){
					echo 'ERROR: '.$e->getMessage();
			}
			$list = '';
			
			foreach($invites as $invite){
				$list .= $invite['friend_id'].',';
			}
			$list = substr($list, 0, -1);
			
			print $list;
			break;
			
		//Write into the database the list of friend IDs from the app request - Called by: functions.js as a response to the appRequestNoDoubles() function
		case 'write_friends':
		
			$fids = $_POST['friends'];
			
			$app->insertDB(APPNAME.'_friends', array('user_fb_id', 'friend_id', 'date', 'time'), array($_SESSION['logged'], $fids, date('Y-m-d'), date('H:i:s')));
			break;
			
		//Save the share clicks of any user - Called by: functions.js on behalf of any page that has an app share button
		case 'write_share':
		
			$data = $_POST['data'];
			
			$app->insertDB(APPNAME.'_shares', array('user_fb_id', 'share_id', 'date', 'time'), array($_SESSION['logged'], $data['post_id'], date('Y-m-d'), date('H:i:s')));
			break;
		
		//Save the form of the user - Called by: end.php(?)
		case 'db_inputres':
		
			if(!$_SESSION['logged']){ $_SESSION['logged'] = 'Unknown'; }
			$data[] = $_POST['name'];
			$data[] = $_POST['surname'];
			$data[] = $_POST['email'];
			$newsletter = $_POST['subscription'];
			$data = date("Y-m-d");
			$data = date("H-i-s");
			foreach($data as $val)
			{
				if($val == '')
				{
					$errors = true;
				}
			}
			if(!$errors)
			{
				$app->insertDB( APPNAME.'_participations', array( 'user_fb_id', 'user_form_name', 'user_form_lastname', 'form_email' ,'user_newslet', 'form_date', 'form_time'), array($_SESSION['logged'], $data[0], $data[1], $data[2], $newsletter, $data[3], $data[4]));
				echo 'ok';
			}
			else
			{
				echo 'error';
			}
			break;
			
	}
	
}
