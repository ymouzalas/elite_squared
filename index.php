<?php

/**
 * Includes the basic scripts and markup for the header.
 */
 
include 'scripts/library.php';

include 'templates/header.php';

if($app->auth() == false && $sets['auth_gate'] == true){ $auth = false; }else{ $auth = true; } //Login check

//Terms of Use page override.

if(isset($_GET['page']) && $_GET['page'] == 'terms'){
	
	include 'templates/terms.php';
	include 'templates/footer.php';
	die();
	
}

/**
 * Chekcs if the user has authorized the app and if not, displays and stops at the login gate.
 */
 
if($auth == false){
	include 'templates/auth.php';
}

/**
 * After the user has liked the page and authorized the app, he is moved around in the rest of the pages
 * using $_GET requests. The variable $_GET['page'] holds the name of the page to be displayed.
 */

if($auth == true && isset($_GET['page'])){
	$page = $_GET['page'];
	include 'templates/'.$page.'.php';
}

/**
 * If the user has come fresh from the authorization link or refreshed the Facebook page he is landed
 * at the first page of the application.
 */

if($auth == true && !isset($_GET['page'])){
	$count = $app->countParticipations(APPNAME.'_participations', $fbuser['id']);
	
	//if($count > 0){ $page = 'congrats'; }else{$page = 'auth';}
	$page = 'auth';
	
	include 'templates/'.$page.'.php';
}

/**
 * Includes the markup for the footer.
 */

include 'templates/footer.php';