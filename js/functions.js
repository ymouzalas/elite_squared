var settings;
	
var req = {};

$(document).ready(function(){
	
	$('#loading').fadeOut(300);
	
  req['req'] = 'stats_request';

  $.post('scripts/helpers/ajax_calls.php', req, function(ret){

    settings = ret.split('2α4ς');

  });
  
});

$(document).ajaxStart(function(){
	$('#loading').fadeIn(100);
});

$(document).ajaxStop(function(){
	$('#loading').fadeOut(300);
});

function authorize(){
	
	var auth = {};
	auth['req'] = 'check_pass';
	auth['pass'] = $('#pass').val();
	
	$.post('scripts/helpers/ajax_calls.php', auth, function(ret){
		
		$('#report').html(ret);
		
	});
	
}

function add_tab(){
	
	FB.ui({
		method: 'pagetab',
		redirect_uri: settings[5]
	}, function(response){console.log(response);});
	
}

/**
 * Basic authorization action to allow access to the user's profile data, based on the scope we choose.
 * Input variables  : perms (string | The text to be displayed in the error message).
 * Output variables :	NONE.
 *
 */

function login(perms){
	FB.login(function(response){
		if (response.authResponse){
			FB.api('/me', {fields: 'first_name,last_name,gender,email'}, function(response) {
				var fbProfile = response;
				var userData = {};
				userData['req'] = 'user_login';
				userData['logged'] = fbProfile.id;
				userData['firstName'] = fbProfile.first_name;
				userData['lastName'] = fbProfile.last_name;
				userData['gender'] = fbProfile.gender;
				userData['email'] = fbProfile.email;
				console.log(userData);
				$.post('scripts/helpers/ajax_calls.php', userData, function(){
					window.location = '?page=game';
				});
			});
		}else{
			window.location = '?page=game';
			//alert('You are not logged in, please make up your mind.');
		}
	}, {scope: perms});
}

/**
 * The error function displays a warning window at the top right of the body for the user to see.
 * Input variables  : text (string | The text to be displayed in the error message).
 * Output variables :	NONE.
 *
 */

function error(text){
	$('#error').remove();
	$('body').prepend('<div id="error"><p><span>'+text+'</span></p></div>');
	$('#error').delay(2000).fadeOut(500);
}

/**
 * The global_redir function redirects the user to some URL outside the Facebook iframe.
 * Input variables  : redirUrl (string | The URL to redirect to).
 * Output variables :	NONE.
 *
 */

function global_redir(redirUrl){
	window.top.location = redirUrl;
}

/**
 * The iframe_redir function redirects the user to some URL inside the Facebook iframe.
 * Input variables  : redirUrl (string | The URL to redirect to).
 * Output variables :	NONE.
 *
 */

function iframe_redir(redirUrl){
	window.location = redirUrl;
}

/**
 * The wallpost function automatically uploads a picture to the user's profile. It is placed
 * in an album named after the application.
 * Input variables  : photoUrl (string | The URL of the picture to be uploaded).
 * Output variables :	NONE.
 *
 */

function wallpost(photoUrl){
	FB.api('/me/photos', 'post', {
		message: '',
		url: photoUrl
	}, postCallback);
}

function postCallback(){
	return;
}

/**
 * The timelineShare function displays the pop-up window of Facebook, in order for the user to share a link to display
 * in their timeline.
 * Input variables  : url (string | The URL to be shared).
 *										name (string | The title of the shared link).
 *										picture (string | The of the picture that will be displayed next to the text).
 *										caption (string | Some text as subtitle).
 *										description (string | A short description of the link shared).
 * Output variables :	NONE.
 *
 */

function timelineShare(){
	var obj = {
		method: 'feed',
		link: settings[6],
		name: settings[9],
		picture: settings[12],
		caption: settings[10],
		description: settings[11]
	};
	
	FB.ui(obj, shareCallback);
}

function shareCallback(data){
	$.post('scripts/helpers/ajax_calls.php', {req: 'write_share', app: settings[14], data: data}, function(ret){ console.log(ret);/*document.location.reload(true);*/ });
}

/**
 * The appRequestReturn function displays the pop-up window of Facebook, in order for the user to choose friends who will recieve a
 * notification/invitation to try out the application. It is STRICTLY connected to its callback function, returnID.
 * appRequestReturn Input variables : text (string | The text that will be displayed in the App Center (255 Characters Max)).
 *																		friends (int | The number of the maximun recipients for each request (20 Max)).
 * Output variables :	NONE.
 *
 */

function appRequestReturn(friends){
	FB.ui({
		method: 'apprequests', 
		message: settings[8],
		max_recipients: friends
	}, returnID);
}

function returnID(data){
	friendID = data.to;
	$('#returnedData').html('<img src="http://graph.facebook.com/'+friendID+'/picture" class="'+friendID+'" />');
}

/**
 * The appRequestNoDoubles function displays the pop-up window of Facebook with the friends for the user to invite but it also sends the IDs
 * of those users to the saveRequests.php file and stores them in the Database (appname_friends table). When the function is called again, the
 * IDs of the previous invites that are stored in the Database (called in with readRequests.php file) are excluded from the pop-up and the user
 * is unable to select them.
 * appRequestReturn Input variables : text (string | The text that will be displayed in the App Center (255 Characters Max)).
 *																		friends (int | The number of the maximun recipients for each request (20 Max)).
 *																		appname (string | The application name you chose when you ran the installation).
 * Output variables :	NONE.
 *
 */

function appRequestNoDoubles(friends){
	var exfriends;
	$.post('scripts/helpers/ajax_calls.php', {req: 'read_friends'}, function(e){
		exfriends = e.split(',');
		
		if(exfriends == ''){exfriends = null}

		FB.ui({
			method: 'apprequests', 
			message: settings[8],
			max_recipients: friends,
			exclude_ids: exfriends
		}, returnID);
	});
}

function returnID(data){
	$.post('scripts/helpers/ajax_calls.php', {req: 'write_friends', app: settings[14], friends: data.to.join()}, function(){ document.location.reload(true); });
}

/**
 * The preload function downloads a number of images from the server to the user's RAM to be shown instantly on mouseover
 * or onclick events.
 * Input variables  : images (string | The file paths seperated by commas).
 * Output variables :	NONE.
 *
 */

function preload(images) {
	if(document.images) {
		var i = 0;
		var imageArray = new Array();
		imageArray = images.split(',');
		var imageObj = new Image();
		for(i=0; i<=imageArray.length-1; i++) {
			imageObj.src=imageArray[i];
		}
	}
}

/**
 * The buildMap function builds a Google Map inside the appointed container.
 * appRequestReturn Input variables : lat (decimal | Latitude of the spot we want to focus on the map).
 *																		long (decimal | Longditude of the spot we want to focus on the map).
 *																		container (string | The ID of the element we want the map to appear in).
 * Output variables :	NONE.
 *
 */

function buildMap(lat,long,container)
{
  var mapProp = {
    center: new google.maps.LatLng(lat,long),
    zoom: 14,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById(container),mapProp);
}
