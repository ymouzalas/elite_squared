CREATE DATABASE  IF NOT EXISTS `elite_squared` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `elite_squared`;
-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: elite_squared
-- ------------------------------------------------------
-- Server version	5.5.46-0ubuntu0.14.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `elite_squared_friends`
--

DROP TABLE IF EXISTS `elite_squared_friends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elite_squared_friends` (
  `user_fb_id` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `friend_id` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY (`user_fb_id`,`friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elite_squared_friends`
--

LOCK TABLES `elite_squared_friends` WRITE;
/*!40000 ALTER TABLE `elite_squared_friends` DISABLE KEYS */;
/*!40000 ALTER TABLE `elite_squared_friends` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elite_squared_likes`
--

DROP TABLE IF EXISTS `elite_squared_likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elite_squared_likes` (
  `page_id` varchar(35) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `page_name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `page_url` varchar(120) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `page_clicks` int(6) NOT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elite_squared_likes`
--

LOCK TABLES `elite_squared_likes` WRITE;
/*!40000 ALTER TABLE `elite_squared_likes` DISABLE KEYS */;
/*!40000 ALTER TABLE `elite_squared_likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elite_squared_participations`
--

DROP TABLE IF EXISTS `elite_squared_participations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elite_squared_participations` (
  `part_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_fb_id` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `user_form_name` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `user_form_lastname` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `form_email` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user_newslet` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `form_date` date NOT NULL,
  `form_time` time NOT NULL,
  PRIMARY KEY (`part_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elite_squared_participations`
--

LOCK TABLES `elite_squared_participations` WRITE;
/*!40000 ALTER TABLE `elite_squared_participations` DISABLE KEYS */;
/*!40000 ALTER TABLE `elite_squared_participations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elite_squared_settings`
--

DROP TABLE IF EXISTS `elite_squared_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elite_squared_settings` (
  `passphrase` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `app_id` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `app_secret` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `like_gate` tinyint(1) NOT NULL,
  `auth_gate` tinyint(1) NOT NULL,
  `page_tab_url` text COLLATE utf8_unicode_ci,
  `shortened_url` text COLLATE utf8_unicode_ci,
  `google_analytics` text COLLATE utf8_unicode_ci,
  `invite_text` text COLLATE utf8_unicode_ci,
  `share_title` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `share_caption` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `share_desc` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `share_img` text COLLATE utf8_unicode_ci,
  `app_terms` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`passphrase`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elite_squared_settings`
--

LOCK TABLES `elite_squared_settings` WRITE;
/*!40000 ALTER TABLE `elite_squared_settings` DISABLE KEYS */;
INSERT INTO `elite_squared_settings` VALUES ('1qazxsw2','1705301069761926','a85a0a126be841f1ed03c13ae83e2a61',0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `elite_squared_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elite_squared_shares`
--

DROP TABLE IF EXISTS `elite_squared_shares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elite_squared_shares` (
  `user_fb_id` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `share_id` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY (`user_fb_id`,`share_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elite_squared_shares`
--

LOCK TABLES `elite_squared_shares` WRITE;
/*!40000 ALTER TABLE `elite_squared_shares` DISABLE KEYS */;
/*!40000 ALTER TABLE `elite_squared_shares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elite_squared_users`
--

DROP TABLE IF EXISTS `elite_squared_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elite_squared_users` (
  `user_fb_id` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `user_firstname` text COLLATE utf8_unicode_ci NOT NULL,
  `user_lastname` text COLLATE utf8_unicode_ci NOT NULL,
  `user_email` text COLLATE utf8_unicode_ci,
  `user_gender` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_link` text COLLATE utf8_unicode_ci NOT NULL,
  `user_dev` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`user_fb_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elite_squared_users`
--

LOCK TABLES `elite_squared_users` WRITE;
/*!40000 ALTER TABLE `elite_squared_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `elite_squared_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-07 13:16:39
