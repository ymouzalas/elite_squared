<?php
	
	require_once '../scripts/library.php';
	
	$dbextract = $conn->prepare('SELECT * FROM '.APPNAME.'_settings');
	$dbextract->execute();
	$appdata = $dbextract->fetch(PDO::FETCH_ASSOC);
	
?>
<!DOCTYPE html>
<html>
 <head>
	<meta charset="utf-8"/>
	<title></title>
	
	 <!-- Bootstrap -->
    <link href="css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="css/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="css/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="css/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="css/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="css/custom.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/style.css"/>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<!--<link rel="stylesheet" href="../css/reset.css">-->
	
	<link rel="stylesheet" href="../css/tooltipster.css"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1" />
	<script src="../js/jquery.tooltipster.min.js"></script>
	<script src="js/admin-panel.js"></script>
	<script type="text/javascript">
	<?php print $sets['google_analytics']; ?>
	</script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.tipup').tooltipster();
	});
	</script>
</head>

  <body class="nav-md">	
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"><span>Application Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
                <img src="images/logo.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>to admin panel</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
             <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3></h3>
                <ul class="nav side-menu">
                  <li class="active"><a href="index.php" ><i class="fa fa-cog"></i> Basic Settings <span class="fa fa-chevron-right"></span></a>
                  </li>
                  <li><a href="social.php"><i class="fa fa-facebook"></i> Social Settings <span class="fa fa-chevron-right"></span></a>
                  </li>
                  <li><a href="terms.php"><i class="fa fa-file-text-o"></i> App Terms of Use <span class="fa fa-chevron-right"></span></a>
                  </li>
                  <li><a href="google_analytics.php"><i class="fa fa-google"></i> Google Analytics <span class="fa fa-chevron-right"></span></a>					
                  </li>
                  <li><a href="databox.php"><i class="fa fa-table"></i> Databox <span class="fa fa-chevron-right"></span></a>                    
                  </li>
                  <li><a href="custom_query.php"><i class="fa fa-list"></i> Custom Query <span class="fa fa-chevron-right"></span></a>                    
                  </li>
                  <li><a href="statistics.php"><i class="fa fa-line-chart"></i> Statistics <span class="fa fa-chevron-right"></span></a>                    
                  </li>
                  <li><a><i class="fa fa-download"></i> Export <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					  <li><a href="export.php">Export all participants</a></li>
					  <li><a href="expunique.php">Export all unique participations</a></li>
                    </ul>
                  </li>
                </ul>
              </div>             

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->            
            <!-- /menu footer buttons -->
          </div>
        </div>     

        <!-- page content -->
        <div class="right_col" role="main">     

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">
					<div class="row x_title">
						<div class="col-md-6">
							<h3>Basic App Settings</h3>
						</div>
					</div>
					<div id="result"><p></p></div>
					<!--<div class="col-md-6">-->
						<div id="basic" class="col-md-offset-3 col-lg-offset-3 col-md-6 col-lg-6">
	
							<h4>Make changes to the basic settings of the application.<br /><small></small></h4>
						
							<div class="input-group">
								<span class="input-group-addon">App ID:</span>
								<input type="text" id="appid" class="form-control" value="<?php print $appdata['app_id'] ?>" />
							</div>
							
							<div class="input-group">
								<span class="input-group-addon">App Secret:</span>
								<input type="text" id="appsecret" class="form-control" value="<?php print $appdata['app_secret'] ?>" />
							</div>
								
							<label class="checkbox inline">
								<input type="checkbox" id="authgate" <?php if($appdata['auth_gate'] == 1){ print 'checked="checked"'; }  ?> value="authgate"> Enable Authorization Gate
							</label>
							
							<div class="input-group">
								<span class="input-group-addon">Page Tab URL:</span>
								<input type="text" id="ptab" class="form-control" value="<?php print $appdata['page_tab_url'] ?>" />
							</div>
							
							<div class="input-group">
								<span class="input-group-addon">Shortened URL:</span>
								<input type="text" id="shorten" class="form-control" value="<?php print $appdata['shortened_url'] ?>" />
							</div>
							
							<div class="row">
								<div id="basic-btn" class="btn btn-primary glyphicon glyphicon-floppy-disk"></div> <div class="btn btn-primary" onclick="add_tab();">Add App to Page Tab</div>
							</div>
	
						</div>
					<!--</div>-->
					<div class="col-md-6">
						<div id="jobtype-table" class="graph general-info-table">	
						
						</div>
					</div>       

                <div class="clearfix"></div>
              </div>
            </div>

          </div>
        
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            KRIS KRIS - "<?php echo DISPLAY; ?>" Admin Panel by <a href="https://xplain.co">Xplain</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
	<script>
      $(document).ready(function() {
		$('.side-menu li a').click(function(e) {
			e.stopPropagation();
			$(this).parent().find('.nav.child_menu').toggle();
		});
	  });
	</script>
  </body>
</html>
