$(document).ready(function(){
if($('#likegate').attr('checked') == undefined){ $('#pages').hide(); }
		
		$('#likegate').click(function(){
			
			if($('#likegate').attr('checked') == undefined){ $('#pages').hide(); }else{ $('#pages').show(); }
		
		});
		
		$('.delpage-btn').click(function(){
			
			var check = confirm('Are you sure you want to delete this page?');
			
			if(check == true){
				
				var pageid = $(this).parent('well').find('input.pageid').val();
				
				console.log(pageid);
				
				var del = {};
				del['req'] = 'delete_page';
				del['pageid'] = pageid;
				
				$.post('../scripts/helpers/ajax_calls.php', del, function(ret){
			
					if(ret == true){
						$('#result p').removeAttr('class').addClass('alert alert-success col-md-12 col-lg-12').html('Page deleted.');
						$(this).parent('blockquote').fadeOut(2000);
					}else{
						$('#result p').removeAttr('class').addClass('alert alert-danger col-md-12 col-lg-12').html('<strong>Warning!</strong> there was an error processing the request. The script reported:<br /><pre>'+ret+'</pre>');
					}
					
				});
					
			}
		
		});
		
		$('.btn-group button').click(function(){
			
			var choice = $(this).attr('id').substring(0, $(this).attr('id').length-5);
			
			hideall();
			
			$('#'+choice).show();
			
		});
		
		$('#basic-btn').click(function(){
			
			var basics = {};
			var pnames;
			var purls;
			var pages = {};
			
			basics['req'] = 'db_in_basics';
			basics['appid'] = $('#appid').val();
			basics['appsecret'] = $('#appsecret').val();
			
			if($('#authgate').attr('checked') == undefined){ basics['authgate'] = 0 }else{ basics['authgate'] = 1 };
			basics['ptab'] = $('#ptab').val();
			basics['shorten'] = $('#shorten').val();
			
			$.post('../scripts/helpers/ajax_calls.php', basics, function(ret){
			
				if(ret == true){
					$('#result p').removeAttr('class').addClass('alert alert-success col-md-12 col-lg-12').html('Settings saved successfully.');
				}else{
					$('#result p').removeAttr('class').addClass('alert alert-danger col-md-12 col-lg-12').html('<strong>Warning!</strong> there was an error processing the request. The script reported:<br /><pre>'+ret+'</pre>');
				}
				
			});
			
		});
		
		$('#addpage-btn').click(function(){
			
			$('#pageswrap').append('<blockquote><div class="input-group"><span class="input-group-addon">Page ID:</span><input type="text" class="form-control pageid" /></div><div class="input-group"><span class="input-group-addon">Name:</span><input type="text" class="form-control pagename" /></div><div class="input-group"><span class="input-group-addon">Page Facebook URL:</span><input type="text" class="form-control pageurl" /></div></blockquote>');
		
		});
		
		$('#social-btn').click(function(){
			
			var social = {};
			
			social['req'] = 'db_in_social';
			social['finvite'] = $('#finvite').val();
			social['title'] = $('#title').val();
			social['caption'] = $('#caption').val();
			social['desc'] = $('#desc').val();
			social['shareimg'] = $('#shareimg').val();
			
			$.post('../scripts/helpers/ajax_calls.php', social, function(ret){
			
				if(ret == true){
					$('#result p').removeAttr('class').addClass('alert alert-success col-md-12 col-lg-12').html('Settings saved successfully.');
				}else{
					$('#result p').removeAttr('class').addClass('alert alert-danger col-md-12 col-lg-12').html('<strong>Warning!</strong> there was an error processing the request. The script reported:<br /><pre>'+ret+'</pre>');
				}
				
			});
			
		});
		
		$('#terms-btn').click(function(){
			
			var terms = {};
			
			terms['req'] = 'db_in_terms';
			terms['terms'] = CKEDITOR.instances['app_terms'].getData();
			
			$.post('../scripts/helpers/ajax_calls.php', terms, function(ret){
			
				console.log(ret);
				if(ret == true){
					$('#result p').removeAttr('class').addClass('alert alert-success col-md-12 col-lg-12').html('Settings saved successfully.');
				}else{
					$('#result p').removeAttr('class').addClass('alert alert-danger col-md-12 col-lg-12').html('<strong>Warning!</strong> there was an error processing the request. The script reported:<br /><pre>'+ret+'</pre>');
				}
				
			});
			
		});
		
		$('#google-btn').click(function(){
			
			var google = {};
			
			google['req'] = 'db_in_google';
			google['google'] = $('#google_code').val();
			
			$.post('../scripts/helpers/ajax_calls.php', google, function(ret){
			
				if(ret == true){
					$('#result p').removeAttr('class').addClass('alert alert-success col-md-12 col-lg-12').html('Settings saved successfully.');
				}else{
					$('#result p').removeAttr('class').addClass('alert alert-danger col-md-12 col-lg-12').html('<strong>Warning!</strong> there was an error processing the request. The script reported:<br /><pre>'+ret+'</pre>');
				}
				
			});
			
		});

	
	
//	draw charts
//total participation per day chart
	$.getJSON("/kriskris/memory_game/admin/partdata.json", function(datajson) {
		console.log(datajson);
		$('#allstats').highcharts({
			title: {
				text: 'App participations per day',
				x: -20 //center
			},
			subtitle: {
				text: '',
				x: -20
			},
			xAxis: {
				categories: datajson['date']
			},
			yAxis: {
				title: {
					text: 'Participations'
				},
				plotLines: [{
					value: 0,
					width: 1,
					color: '#808080'
				}]
			},
			tooltip: {
				valueSuffix: ' people'
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'middle',
				borderWidth: 0
			},
			series: [{
				name: 'Participants',
				data: datajson['number']
			}]
			});
		});
	
//	unique participation per day chart
	$.getJSON("/kriskris/memory_game/admin/uniquedata.json", function(uniquejson) {
		console.log(uniquejson);
		$('#uniquestats').highcharts({
			title: {
				text: 'App unique participations per day',
				x: -20 //center
			},
			subtitle: {
				text: '',
				x: -20
			},
			xAxis: {
				categories: uniquejson['dates']
			},
			yAxis: {
				title: {
					text: 'Unique Participations'
				},
				plotLines: [{
					value: 0,
					width: 1,
					color: '#808080'
				}]
			},
			tooltip: {
				valueSuffix: ' people'
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'middle',
				borderWidth: 0
			},
			series: [{
				name: 'Unique Participants',
				color: 'green',
				data: uniquejson['numbers']
			}]
			});
		});	    
    
    
    
    




});
