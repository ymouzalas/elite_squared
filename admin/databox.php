<?php
	
	require_once '../scripts/library.php';
	
	$dbextract = $conn->prepare('SELECT * FROM '.APPNAME.'_settings');
	$dbextract->execute();
	$appdata = $dbextract->fetch(PDO::FETCH_ASSOC);
	
?>
<!DOCTYPE html>
<html>
 <head>
	<meta charset="utf-8"/>
	<title></title>
	
	 <!-- Bootstrap -->
    <link href="css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="css/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="css/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="css/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="css/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="css/custom.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/style.css"/>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<!--<link rel="stylesheet" href="../css/reset.css">-->
	
	<link rel="stylesheet" href="../css/tooltipster.css"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1" />
	<script src="../js/jquery.tooltipster.min.js"></script>
	<script src="js/admin-panel.js"></script>
	<script type="text/javascript">
	<?php print $sets['google_analytics']; ?>
	</script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.tipup').tooltipster();
	});
	</script>
</head>

  <body class="nav-md">	
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"><span>Application Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
                <img src="images/logo.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>to admin panel</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
             <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3></h3>
                <ul class="nav side-menu">
                  <li><a href="index.php" ><i class="fa fa-cog"></i> Basic Settings <span class="fa fa-chevron-right"></span></a>
                  </li>
                  <li><a href="social.php"><i class="fa fa-facebook"></i> Social Settings <span class="fa fa-chevron-right"></span></a>
                  </li>
                  <li><a href="terms.php"><i class="fa fa-file-text-o"></i> App Terms of Use <span class="fa fa-chevron-right"></span></a>
                  </li>
                  <li><a href="google_analytics.php"><i class="fa fa-google"></i> Google Analytics <span class="fa fa-chevron-right"></span></a>					
                  </li>
                  <li class="active"><a href="databox.php"><i class="fa fa-table"></i> Databox <span class="fa fa-chevron-right"></span></a>                    
                  </li>
                  <li><a href="custom_query.php"><i class="fa fa-list"></i> Custom Query <span class="fa fa-chevron-right"></span></a>                    
                  </li>
                  <li><a href="statistics.php"><i class="fa fa-line-chart"></i> Statistics <span class="fa fa-chevron-right"></span></a>                    
                  </li>
                  <li><a><i class="fa fa-download"></i> Export <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="export.php">Export all participants</a></li>
					  <li><a href="expunique.php">Export all unique participations</a></li>
                    </ul>
                  </li>
                </ul>
              </div>             

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->            
            <!-- /menu footer buttons -->
          </div>
        </div>     

        <!-- page content -->
        <div class="right_col" role="main">     

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">
					<div class="row x_title">
						<div class="col-md-6">
							<h3>Databox</h3>
						</div>
					</div>
					<!--<div class="col-md-6">-->
					<div id="result"><p></p></div>
						<div id="databox" class="col-md-12 col-lg-12">
	
                        <?php
                            
                            try{
                                $select = 'SELECT *, form_email as usermail FROM '.APPNAME.'_participations';
                                $playercount = $conn->prepare($select);
                                $playercount->execute();
                                $players = $playercount->rowCount();
                                $playerinfo = $playercount->fetchAll();
                            }catch(PDOException $e){
                                print 'ERROR: '.$e->getMessage();
                            }
                            try{
                                $sumselect = 'SELECT COUNT(form_email) FROM '.APPNAME.'_participations Group by form_email';
                                $uniqueplayer = $conn->prepare($sumselect);
                                $uniqueplayer->execute();
                                $unique = $uniqueplayer->rowCount();
                                $uniqueinfo = $uniqueplayer->fetchAll();
                                
                            }catch(PDOException $f){
                                print 'ERROR:' .$f->getMessage();
                            }
                            
                            try{
                                $shareselect = 'SELECT * FROM '.APPNAME.'_shares';
                                $shareplayer = $conn->prepare($shareselect);
                                $shareplayer->execute();
                                $shares = $shareplayer->rowCount();
                                $shareinfo = $shareplayer->fetchAll();
                                
                            }catch(PDOException $f){
                                print 'ERROR:' .$f->getMessage();
                            }
                            
                            
                            try{
                                $invselect = 'SELECT * FROM '.APPNAME.'_friends';
                                $invplayer = $conn->prepare($invselect);
                                $invplayer->execute();
                                $invites = $invplayer->rowCount();
                                $invinfo = $invplayer->fetchAll();
                                
                            }catch(PDOException $f){
                                print 'ERROR:' .$f->getMessage();
                            }
                            
                        ?>
                        
                            <h4>Information stored in the database.<br /><small></small></h4>
                            
                            <p>Σύνολο συμμετεχόντων: <strong><?php print $players; ?></strong></p>
                            <p>Σύνολο unique συμμετεχόντων: <strong><?php print $unique; ?></strong></p>
                            <p>Σύνολο κοινοποιήσεων: <strong><?php print $shares; ?></strong></p>
                            <p>Invites: <strong><?php print $invites; ?></strong></p>
                            
                            <h5>Λίστα συμμετεχόντων</h5>
                            <div id="hide" class="btn btn-info">εμφάνιση/απόκρυψη</div>
                            <table id="usrs" class="table table-striped">
                                <tr><th>Ονοματεπώνυμο</th><th>E-mail</th><th>Newsletter</th><th>Game Time (seconds)</th><th>Submission Date</th><th>Submission Time</th><th>Προφίλ Facebook</th></tr>
                                <?php 
                                
                                    foreach($playerinfo as $player){
                                        print '<tr><td>'.$player["user_form_name"].' '.$player["user_form_lastname"].'</td><td>'.$player["usermail"].'</td><td>'.$player["user_newslet"].'</td><td>'.$player["game_time"].'</td><td>'.$player["form_date"].'</td><td>'.$player["form_time"].'</td><td><a href="https://www.facebook.com/'.$player["user_fb_id"].'" target="_blank">Facebook Profile</a></td></tr>';
                                    }
                                
                                ?>
                            </table>
                            
                        
                        </div>
                        
                        <!-- /Databox -->
					<!--</div>-->
					<div class="col-md-6">
						<div id="jobtype-table" class="graph general-info-table">	
						
						</div>
					</div>       

                <div class="clearfix"></div>
              </div>
            </div>

          </div>
        
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            KRIS KRIS - "<?php echo DISPLAY; ?>" Admin Panel by <a href="https://xplain.co">Xplain</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
	<script>
      $(document).ready(function() {
		$('.side-menu li a').click(function(e) {
			e.stopPropagation();
			$(this).parent().find('.nav.child_menu').toggle();
		});
        
        
        $('#hide').click(function(){
			$('#usrs').toggle();
		});
        
        
	  });
	</script>
  </body>
</html>
