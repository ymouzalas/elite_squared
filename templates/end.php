<?php

	if(!(isset($_SESSION['logged']))){
		echo '<script>window.location = "?page=auth"</script>';	
	}
?>

<div id="endwrapper">
    
    <div id="submit">
		<h3>Congratulations! Please enter your details in order to participate.</h3>
		<label id="nlabel">Name*</label><input type="text" class="form-control" name="name" id="name" required autofocus/><div class="first-name-error field-error"></div>
		<label id="slabel">Lastname*</label><input type="text" class="form-control" name="surnname" id="surname" required autofocus/><div class="last-name-error field-error"></div>
		<label id="elabel">Email*</label><input type="text" class="form-control" name="email" id="email" required autofocus><div class="email-error field-error"></div>
		<button id="submit-button" class="submit-btn btn" name="submit" value="Submit new">Submit</button>
		<label id="sublabel" style="font-size:10px;"><input type="checkbox" name="subscription" id="subscription" value="no"/> Subscribe to newsletter.</label>
	</div>

</div>
	
	



<script type="text/javascript">
$(document).ready(function(){
jQuery('#submit-button').click(function() {

    var errorsSelector = {
		firstName: $('.first-name-error'),
		lastName: $('.last-name-error'),
		email: $('.email-error'),
    };
	errorsSelector.firstName.empty();
	errorsSelector.lastName.empty();
	errorsSelector.email.empty();
	
    var errors = false;
	var userData = {};
	userData['req'] = 'db_inputres';
	userData['name'] = $('#name').val().trim();
	userData['surname'] = $('#surname').val().trim();
	userData['email'] = $('#email').val().trim();
	
	
	if($( "#subscription").is(':checked'))
	{
		userData['subscription'] = 'yes';
	}
	else
	{
		userData['subscription'] = 'no';
	}
	//name field
	if(userData['name'] == '' || userData['name'] == null)
	{
		errors = true;
		errorsSelector.firstName.empty().html('Το πεδίο είναι υποχρεωτικό').css("color","red");
	}

	//lastname field
	if(userData['surname'] == '' || userData['surname'] == null)
	{
		errors = true;
		errorsSelector.lastName.empty().html('Το πεδίο είναι υποχρεωτικό').css("color","red");
	}	

	//email field
	if(userData['email'] == '' || userData['email'] == null || !isEmail(userData['email']))
	{
		errors = true;
		errorsSelector.email.empty().html('Το πεδίο είναι υποχρεωτικό, παρακαλώ εισάγετε email με σωστή μορφή.').css("color","red");
	}	
	
	if (!errors) {
		console.log(userData);
		$.post('scripts/helpers/ajax_calls.php', userData, function(ret)
		{
			if(ret =='ok')
			{
				alert(ret);
				window.location = '?page=congrats';
			}
			else
			{
				alert('Something went wrong');
			}

		});
        
	}else{
        console.log("error!");
        
    }
});	
	
	


function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
	
	
});

</script>