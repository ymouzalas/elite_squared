<!DOCTYPE html>

<html>
<head>
	<meta charset="utf-8"/>
	<title></title>
	<link rel="stylesheet" href="css/style.css"/>
	<link rel="stylesheet" href="css/tooltipster.css"/>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!--	import highcharts-->
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1" />
	<script src="js/jquery.tooltipster.min.js"></script>
	<script type="text/javascript">
	<?php print $sets['google_analytics']; ?>
	</script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.tipup').tooltipster();
	});
	</script>
</head>

<body>
	<div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '<?php print $sets['app_id']; ?>',
          xfbml      : true,
          status     : true,
          cookie     : true,
          version    : 'v2.2'
        });

        FB._https = true;

        FB.Canvas.setAutoGrow();
        // ADD ADDITIONAL FACEBOOK CODE HERE
      };

      (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>
	
	<div id="loading">
		<center>
			<div class="spinner">
				<div class="dot1"></div>
				<div class="dot2"></div>
			</div>
			<!-- <img src="img/loading.gif" alt="Loading..." width="150" /> -->
		</center>
	</div>
