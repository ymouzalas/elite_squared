 
<div id="likewrapper" class="container">
	
	<div class="row">
		
		<div id="likebox" class="col-md-offset-1 col-lg-offset-1 col-sm-6 col-md-4 col-lg-4">
			<center>
			<?php
			
				$dbextract = $conn->prepare('SELECT page_url FROM '.APPNAME.'_likes');
				$dbextract->execute();
				$pagedata = $dbextract->fetchAll();
				$pagedata = array_reverse($pagedata);
				$logo = 1;
				
				foreach($pagedata as $page){
					
					print '<div class="like-btn row"><div class="fb-like" data-href="'.$page['page_url'].'" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></div>';
					
					$logo++;
					
				}
			
			?></center>
		</div>
		
	</div>
	
</div>