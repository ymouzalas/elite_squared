<?php

	if(!(isset($_SESSION['logged']))){
		//echo '<script>window.location = "?page=auth"</script>';	
	}

	$dev = $_SERVER['HTTP_USER_AGENT'];

	$app->insertDB(APPNAME.'_users', array('user_fb_id', 'user_firstname', 'user_lastname', 'user_email', 'user_gender', 'user_link', 'user_dev'), array($fbuser['id'], $fbuser['first_name'], $fbuser['last_name'], $fbuser['email'], $fbuser['gender'], $fbuser['profileURL'], $dev));
	
	$data = array (
		
					array (
								'question'=> "Ποιες είναι οι 2 νέες γεύσεις των Elite Crackers;",
								'answers' => array(
														"Α. Σίτου με φύτρο σταριού και με Μυρωδικά",
														"Β. Με Σίκαλη και νιφάδες βρώμης και με Θαλασσινό αλάτι"
											
												),
								'correct' => 0,
						),
					array (
								'question' => "Τα νέα Elite Crackers Σίτου με φύτρο σταριού είναι πλούσια σε...",
								'answers'  => array(
														"Α. Λιπαρά",
														"Β. Φυτικές ίνες"
													),
								'correct' => 1,
						),
					array (
								'question'=> "Τα νέα Elite Crackers Σίκαλης με νιφάδες βρώμης είναι πλούσια σε:",
								'answers' => array(
														"Α. Φυτικές ίνες",
														"Β. Ζάχαρη"
													),
								'correct' => 0,
						),
					array (
								'question'=> "Τα νέα Elite Crackers είναι απολαυστική επιλογή για...",
								'answers' => array(
														"Α. Το πρωινό",
														"Β. Κάθε στιγμή της μέρας"																											
													),
								'correct' => 1,
						),
					array (
								'question'=> "Τα νέα Elite Crackers είναι η καλύτερη παρέα...",
								'answers' => array(
														"A. Παντού",
														"B. Στις βόλτες σου στην πόλη",																							
													),
								'correct' => 0,
						),
					array (
								'question'=> "Σε ποιους Ολυμπιακούς Αγώνες πήρα την 5η θέση;",
								'answers' => array(
														"Α. Του Λονδίνου (2012)",
														"Β. Του Σίδνεϊ (2000)"														
													),
								'correct' => 0,
						),
					array (
								'question'=> "Ο σημαντικότερος λόγος που επιλέγω τα νέα Elite Crackers είναι…",
								'answers' => array(
														"Α. Γιατί είναι πλούσια τόσο σε φακές",
														"Β. Γιατί ταιριάζουν με τον υγιεινό τρόπο ζωής μου"
														
													),
								'correct' => 1,
						),
					array (
								'question'=> "Ποια είναι η πιο πρόσφατη διάκρισή μου;",
								'answers' => array(
														"Α. Ασημένιο στη Μόσχα το Μάρτιο 2014",
														"Β. Χάλκινο στους Μεσογειακούς το 2013"													
													),
								'correct' => 0,
						),
					array (
								'question'=> "Τα νέα Elite Crackers Σίτου με φύτρο σταριού έχουν τόσες θερμίδες ανά κράκερ όσο",
								'answers' => array(
														"Α. Ένα κομμάτι πίτσας",
														"Β. Λιγότερες από όλα τα παραπάνω: μόνο 26 θερμίδες!"
													),
								'correct' => 1,
						),
					array (
								'question'=> "Τα νέα Elite Crackers με Σίκαλη & νιφάδες βρώμης έχουν τόσες θερμίδες ανά κράκερ όσο...",
								'answers' => array(
														"Α. ιγότερες από όλα τα παραπάνω: μόνο 26 θερμίδες!",
														"Β. Ένας κουραμπιές"														
													),
								'correct' => 0,
						),
	);

?>

<div id="gamewrapper">
	<!-- question wrapper this is where a 4 random questions are called and printed out in the div -->
	<?php
		$num=range(0,9);
		shuffle($num);
		for ($i=0; $i<4; $i++) {      
			$result[$i]=$num[$i];    
		}    
		sort($result);
		$first  = $result[0];
		$second = $result[1];
		$third  = $result[2];
		$fourth = $result[3];
		//the foreach loop called below is where the questions and their multiple choices are printed out dynamically into the divs.
		foreach($result AS $key=>$value){
			echo '<div id="quizwrap_'.$key.'" class="quizwrap">';
			echo '<div id="questions" class="quest">';
			echo '<h4> Ερώτηση '. $key+1 .':<br><br>'.$data[$value]['question'].'</h4>';
			echo '</div>';
			//print multiple choice
			echo '<div id="answers" class="ans">';
				foreach($data[$value]['answers'] AS $k=>$v){
					echo '<div id="choice_'.$key.'" class="btn-choice"><a href="#" name="answr btn" class="answer answr_'.$k.'" data-qid="'.$key.'" data-id="'.$k.'"> ';
					print_r($v);
					echo '</a></div>';
				}
			echo '</div>';
			echo '</div>';
		}
	?>
	<div id="win-msg"  class="win-msg">Συγχαρητήρια Κερδίσατε!!</div>
	<div id="wrg-msg"  class="wrg-msg">Λάθος Απάντηση!</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('.win-msg').hide();
	$('.wrg-msg').hide();
	
	var correct = ['<?php echo $data[$first]['correct'];?>', '<?php echo $data[$second]['correct'];?>','<?php echo $data[$third]['correct'];?>', '<?php echo $data[$fourth]['correct'];?>'];
	//		hides all questions and answers except for first one
	for(var j=1;j<4;j++){
		$('#quizwrap_'+j).hide();
	}
	var count = 0;
	var score = 0;
	//	gets the value of the answer chosen
	$('.answer').on('click', function(e) {
		e.preventDefault();
		var place = $(this).attr('data-qid');
		var solution = $(this).attr('data-id');
		//	hides correctly answered question and makes next one appear. Also compares chosen answer to correct answer.
		if (solution==correct[place]) {
				$('#quizwrap_'+count).slideUp(1200);
				//$('#quizwrap_'+count).fadeOut(1000); alternate animation fade
				count++;
				//$('#quizwrap_'+count).fadeIn(1000); alternate animation fade
				$('#quizwrap_'+count).slideDown(1200);
				score++;
		}else{
		// prints wrong answer message and plays fencing strike sound with jplayer 2
				$('.wrg-msg').fadeIn(500);
				//$("#jquery_jplayer_2").jPlayer("play");			
				$('.wrg-msg').fadeOut(1500);
		}	
		// after user answers 4 questions correctly, sends him to the form page.
		if (score==4) {
			$('.win-msg').fadeIn(1500);
			// time delayed redirect
			$('.win-msg').html('<iframe width="640" height="360" src="https://www.youtube.com/embed/QlF4rhAbwyc?rel=0&autoplay=1&rel=0&controls=0" frameborder="0" allowfullscreen></iframe>');
			//window.setTimeout(function() {
			//	window.location.href = '?page=end';
			//}, 1500);
		}
	});
});

</script>
